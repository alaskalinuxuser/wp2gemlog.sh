#!/bin/bash
# This script depends on html2text, jq. It is used to get a rss feed from a wordpress blog, and then
# programatically turn it into a gemlog.

# We need to get the feed: Replace with your wordpress blog website.
curl https://alaskalinuxuser3.ddns.net/feed/ > rssObject

# We need to turn it from xml into json.
cat rssObject | xq . > rss.json

# We need to modify the json text a little.
sed -i 's/content\:encoded/contentencoded/g' ./rss.json

# Get the title of the blog/channel and make the file.
jq '.rss' ./rss.json | jq '.channel' |jq '.title' | cut -c 2- |sed 's/.$//' > output.gemini
sed -i 's/^/\# /' ./output.gemini

# And add the channel description.
jq '.rss' ./rss.json | jq '.channel' |jq '.description' | cut -c 2- |sed 's/.$//' >> output.gemini

# Echo a blank line
echo "   " >> output.gemini

# And Get the items.
# Start a loop, Wordpress RSS feeds only do up to 10 RSS items.
number=1

while [ $number -le 9 ]
do

# Get the title to a temp file.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.title' | cut -c 2- |sed 's/.$//' > tempoutput.gemini
# Make it bold.
sed -i 's/^/\## /' ./tempoutput.gemini
# And put it in the other file.
cat tempoutput.gemini >> output.gemini
# And the content.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.contentencoded' | cut -c 2- |sed 's/.$//' | html2text >> output.gemini
echo "   " >> output.gemini

((number++))

done

# Fix formatting issues:
#1. Remove \" and replace with "
sed -i 's/\\\"/\"/g' ./output.gemini
#2. Fix [&#8230;] with ...
sed -i 's/\[\&\#8230\;\]/\.\.\./g' ./output.gemini
#3. Fix &#8217; with apostrophe.
sed -i "s/\&\#8217\;/\'/g" ./output.gemini
#4. Fix &#8211; with dash.
sed -i "s/\&\#8211\;/\-/g" ./output.gemini
#5. Fix \n\n\n\n with new blank line.
sed -r -i 's/\\n\\n\\n\\n/\n/g' ./output.gemini

# Copy your new page to your gemlog.
cp ./output.gemini /home/gemini/gemini/index.gmi

#remove old files. Comment these lines out if you need to debug.
rm rssObject
rm rss.json
rm tempoutput.gemini
rm output.gemini

# Exit when done.
exit 0
