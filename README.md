# wp2gemlog
A bash script to automatically convert your wordpress blog's RSS feed into a gemlog.

# Pre-requisites
- Install a gemini server, I used agate.
- Install yq (for xq,jq) , html2text, curl, and then the usual tools like sed, echo, cat, and cut.

# Instructions

Decide which layout you want, edit the website and file location information, and set a cron job to run the script. Here are the functioning supported layouts:

- wp_rss_parse_onepage.sh
This method will create one home page from your rss feed. All (up to) 10 most recent articles from your WP blog will display here. Note that when run, it will overwrite old entries and only display the current 10 most recent entries.

- wp_rss_parse_multi.sh
This method will create 10 linked gemini pages (one for each rss feed article) and one main page with the links on them. Note that when run, it will overwrite old entries and only display the current 10 most recent entries. This works well to just display the 10 latest articles.

- wp_parse_multi.sh
This method will create linked gemini pages (one for each article) and one main page with the links on them that grows with each new article. You can call the script via cron, when it runs it will attached the newest article as a post with links and create a new file for the new feed link. It is not smart, in that it just takes the latest post, it doesn't check to see how many posts were made between calls. This works good if I make a new post every Thursday, and have a cron job to run this every Friday.

# Work in progress

- wp_parse_onepage.sh
This method will create one homepage that grows with each article you write.

- wp_homepage.sh
This method will recreate your wp home page in a gemlog form. The idea is for people who have a static home page.

- wp_most_recent.sh
This method will create one gemlog page which only displays your most recent article. This article will be replaced with the new most recent article when the cron job runs again.

# Issues

- For blogs featuring code, such as java or bash shell script code, it will be displayed as is, but the gemini server may mistake symbols such as the pound sign (hashtag) as a new heading, etc.
- There is no inline pictures support in gemini servers (that I know of).
- Pictures from your articles do not appear as clickable links, just links to the web. It would be nice if the pictures were downloaded, stored, and then re-linked in the gemini server.
