#!/bin/bash
# This script depends on html2text, jq. It is used to get a rss feed from a wordpress blog, and then
# programatically turn it into a gemlog.

#--------------------------------------------------------------------
# Downloading the RSS feed from your wordpress blog.
#--------------------------------------------------------------------
# We need to get the feed: Replace with your wordpress blog website.
curl https://alaskalinuxuser3.ddns.net/feed/ > ./rssObject

# We need to turn it from xml into json.
cat ./rssObject | xq . > ./rss.json

# We need to modify the json text a little.
sed -i 's/content\:encoded/contentencoded/g' ./rss.json


#--------------------------------------------------------------------
# Get the latest post. Note, this is not smart. It will keep creating
# a file for the latest post every time this is called. So only run
# this manually when you post, or run this on a cron job when you 
# have regular, steady posts.
#--------------------------------------------------------------------

# Let's Get the items.
# Start a loop, Wordpress RSS feeds only do up to 10 RSS items.
number=1
echo "   " > ./linkoutput.gemini


while [ $number -le 1 ]
do
# Make the link file.
# Get the date:
date "+%F.gmi " > ./tempoutput.gemini
# Make it a link.
sed -i 's/^/\=\> /' ./tempoutput.gemini
# And add the date to make it "clickable".
date "+%F " >> ./tempoutput.gemini
# Get the title to a temp file.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.title' | cut -c 2- |sed 's/.$//' >> ./tempoutput.gemini
# And put the lines together.
sed ':a; N; $!ba; s/\n//g' ./tempoutput.gemini > ./newoutput.gemini
mv ./newoutput.gemini ./tempoutput.gemini
# and a blank line.
echo "   " >> ./tempoutput.gemini

# And put it in the other file.
cat ./tempoutput.gemini >> ./linkoutput.gemini

# Get the title to a temp file.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.title' | cut -c 2- |sed 's/.$//' > ./tempoutput.gemini
# Make it bold.
sed -i 's/^/\## /' ./tempoutput.gemini

# And the content.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.contentencoded' | cut -c 2- |sed 's/.$//' | html2text >> ./tempoutput.gemini
echo "   " >> ./tempoutput.gemini

# Fix formatting issues:
#1. Remove \" and replace with "
sed -i 's/\\\"/\"/g' ./tempoutput.gemini
#2. Fix [&#8230;] with ...
sed -i 's/\[\&\#8230\;\]/\.\.\./g' ./tempoutput.gemini
#3. Fix &#8217; with apostrophe.
sed -i "s/\&\#8217\;/\'/g" ./tempoutput.gemini
#4. Fix &#8211; with dash.
sed -i "s/\&\#8211\;/\-/g" ./tempoutput.gemini
#5. Fix \n\n\n\n with new blank line.
sed -r -i 's/\\n\\n\\n\\n/\n/g' ./tempoutput.gemini

# and put it all into the right name.
cat ./tempoutput.gemini > ./`date +%F`.gmi

# Add another number
((number++))

done

#--------------------------------------------------------------------
# Check to see if your file exists. If it doesn't, create it, if it 
# does, don't destroy it, just add the links.
#--------------------------------------------------------------------

#FILE=/home/alaskalinuxuser/Downloads/gemini/index.gmi
FILE=/home/gemini/gemini/index.gmi

if [ -f "$FILE" ]; then
    cat ./linkoutput.gemini >> $FILE
else 
    # Get the title of the blog/channel and make the file.
	jq '.rss' ./rss.json | jq '.channel' |jq '.title' | cut -c 2- |sed 's/.$//' > ./output.gemini
	sed -i 's/^/\# /' ./output.gemini

	# And add the channel description.
	jq '.rss' ./rss.json | jq '.channel' |jq '.description' | cut -c 2- |sed 's/.$//' >> ./output.gemini

	# Echo a blank line
	echo "   " >> ./output.gemini
	cat ./linkoutput.gemini >> ./output.gemini
	
	# Fix formatting issues:
    #1. Remove \" and replace with "
    sed -i 's/\\\"/\"/g' ./output.gemini
    #2. Fix [&#8230;] with ...
    sed -i 's/\[\&\#8230\;\]/\.\.\./g' ./output.gemini
    #3. Fix &#8217; with apostrophe.
    sed -i "s/\&\#8217\;/\'/g" ./output.gemini
    #4. Fix &#8211; with dash.
    sed -i "s/\&\#8211\;/\-/g" ./output.gemini
    #5. Fix \n\n\n\n with new blank line.
    sed -r -i 's/\\n\\n\\n\\n/\n/g' ./output.gemini
    # And make this the new index page.
	mv ./output.gemini ./index.gmi
fi

# Copy your new page to your gemlog.
#mv ./*.gmi /home/alaskalinuxuser/Downloads/gemini/
mv ./*.gmi /home/gemini/gemini/

#remove old files. Comment these lines out if you need to debug.
rm ./rssObject
rm ./rss.json
rm ./tempoutput.gemini
rm ./linkoutput.gemini

# Exit when done.
exit 0
