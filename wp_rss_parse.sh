#!/bin/bash
# This script depends on html2text, jq.

# We need to get the feed:

# We need to modify the json text a little.
sed -i 's/content\:encoded/contentencoded/g' ./rss.json

# Get the title of the blog/channel and make the file.
jq '.rss' ./rss.json | jq '.channel' |jq '.title' | cut -c 2- |sed 's/.$//' > output.gemini

# And add the channel description.
jq '.rss' ./rss.json | jq '.channel' |jq '.description' | cut -c 2- |sed 's/.$//' >> output.gemini

# Echo a blank line
echo "   " >> output.gemini

# And Get the items.
# Start a loop, Wordpress RSS feeds only do up to 10 RSS items.
number=1

while [ $number -le 10 ]
do

# Get the title.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.title' | cut -c 2- |sed 's/.$//' >> output.gemini
# And the description.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.description' | cut -c 2- |sed 's/.$//' >> output.gemini
# And the content.
jq '.rss' ./rss.json | jq '.channel' | jq  --arg number $number -r '.item['$number']' | jq '.contentencoded' | cut -c 2- |sed 's/.$//' | html2text >> output.gemini
echo "   " >> output.gemini

((number++))

done




# Fix formatting issues:
#1. Remove \" and replace with "
sed -i 's/\\\"/\"/g' ./output.gemini
#2. Fix [&#8230;] with ...
sed -i 's/\[\&\#8230\;\]/\.\.\./g' ./output.gemini
#3. Fix &#8217; with '
sed -i "s/\&\#8217\;/\'/g" ./output.gemini
#4. Fix &#8211; with -
sed -i "s/\&\#8211\;/\-/g" ./output.gemini
#5. Fix \n\n\n\n with blank line.
sed -r -i 's/\\n\\n\\n\\n/\n/g' ./output.gemini

# Exit when done.
exit 0


